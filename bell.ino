#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

// сеть 
const char* ssid = "www.mplmurmansk.ru";
const char* password = "CamelionACCU";

MDNSResponder mdns;
ESP8266WebServer server(80);
String webPage = "";

void setup(void){
  webPage += "<h1>ESP8266 Авт. звонок</h1>";
  
  // реле
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);

  // коннектимся
  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // ждем соединения
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // подключились
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  server.on("/", [](){
    server.send(200, "text/html", webPage);
  });

  server.on("/call", [](){
    if (server.hasArg("plain") == false) {
      server.send(200, "text/plain", "Body not received");
      return;
    }
    
    server.send(200, "text/html", webPage);
    digitalWrite(5, HIGH);
    delay(1000);
  });

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}
